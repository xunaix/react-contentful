import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from "react-router-dom";
import App from './App';
import Article from './Article'

ReactDOM.render(
  <Router>
    <Route exact path='/' component={App} />
    <Route path='/:slug' component={Article} />
  </Router>
  , document.getElementById('root'));
