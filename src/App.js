import React from 'react';
import client from './service/client'

class App extends React.Component {
  constructor () {
    super()
    this.state = {
      posts: []
    }
  }

  componentDidMount () {

    client
      .getEntries({content_type: 'blogPost'})
      .then(response => {
        this.setState({
          posts: response.items
        })
      })
      .catch(err => console.log(err));
  }

  render () {
    const { posts } = this.state
  return (
    <div className="App">
      {posts.map((post, i) => (
        <a href={post.fields.slug} key={i}>
          <h2>{post.fields.title}</h2>
          <img src={post.fields.heroImage.fields.file.url + '?w=300'} alt={post.fields.heroImage.fields.description} />
        </a>
      ))}
    </div>
  );
  }
}

export default App;
