import React from 'react'
import client from './service/client'
import marked from 'marked'

class Article extends React.Component {
  constructor () {
    super()
    this.state = {
      post: null
    }
  }

  componentDidMount () {
    const { match } = this.props
    if (match.params && match.params.slug) {
      client.getEntries({content_type: 'blogPost', 'fields.slug': match.params.slug})
        .then(res => this.setState({post: res.items[0]}))
    }
  }

  getParsedMarkdown (content) {
    return {
      __html: marked(content, {sentize: true})
    }
  }

  render () {
    const { post } = this.state
    if ( post == null ) {
      return <h1>Loading ...</h1>
    }
    return (
      <div>
        <h1>{post.fields.title}</h1>
        <p>{post.fields.description}</p>
        <p dangerouslySetInnerHTML={this.getParsedMarkdown(post.fields.body)} />
      </div>
    )
  }
}

export default Article
